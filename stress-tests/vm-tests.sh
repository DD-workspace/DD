#!/bin/sh -eu

LOG_FILE="${LOG_FILE:-vm-test.log}"

SYSBENCH="$(command -v sysbench)"

# Save stderr as well
exec 2>&1

echo "$(date)" | tee "${LOG_FILE}"

if [ -z "${SYSBENCH}" ]; then
	echo "Skipping: sysbench tests (try: apt install sysbench)" | \
		tee -a "${LOG_FILE}"
else
	printf "\n\nfileio\n\n" | tee -a "${LOG_FILE}"
	"${SYSBENCH}" fileio prepare --file-test-mode=rndrw --threads=4 --time=60 | tee -a "${LOG_FILE}"
	"${SYSBENCH}" fileio run     --file-test-mode=rndrw --threads=4 --time=60 | tee -a "${LOG_FILE}"
	printf "\n\ncpu\n\n" | tee -a "${LOG_FILE}"
	"${SYSBENCH}" cpu    run --threads=4 --time=60 | tee -a "${LOG_FILE}"
	printf "\n\nmemory\n\n" | tee -a "${LOG_FILE}"
	"${SYSBENCH}" memory run --threads=4 --time=60 | tee -a "${LOG_FILE}"
fi
# Perform basic OpenSSL tests too
printf "\n\nCPU OpenSSL\n\n" | tee -a "${LOG_FILE}"
openssl speed --seconds 60 sha256 | tee -a "${LOG_FILE}"
