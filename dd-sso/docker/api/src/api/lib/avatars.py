#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import logging
import os
import pprint
import time
import traceback
from datetime import datetime, timedelta

import yaml
from keycloak import KeycloakAdmin

from typing import cast


class Avatars:
    def __init__(
        self,
        url: str="http://dd-sso-keycloak:8080/auth/",
        username: str=os.environ["KEYCLOAK_USER"],
        password: str=os.environ["KEYCLOAK_PASSWORD"],
        realm: str="master",
        verify: bool=True,
    ) -> None:
        self.url = url
        self.username = username
        self.password = password
        self.realm = realm
        self.verify = verify

    def connect(self) -> None:
        self.keycloak_admin = KeycloakAdmin(
            server_url=self.url,
            username=self.username,
            password=self.password,
            realm_name=self.realm,
            verify=self.verify,
        )

    def get_user_avatar(self, username: str) -> str:
        self.connect()
        return cast(str, self.keycloak_admin.get_user_id(username))


# # Add user
# new_user = keycloak_admin.create_user({"email": "example@example.com",
#                     "username": "example@example.com",
#                     "enabled": True,
#                     "firstName": "Example",
#                     "lastName": "Example"})
# print(new_user)
