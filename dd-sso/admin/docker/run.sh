#!/bin/sh
#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# We possibly need to fix bad old permissions
chown -R www-data:www-data \
        /admin/custom \
        /admin/moodledata/saml2 \
	"${DATA_FOLDER}" \
        "${LEGAL_PATH}" \
        "${NC_MAIL_QUEUE_FOLDER}"

cd /admin
export PYTHONWARNINGS="ignore:Unverified HTTPS request"
exec su -s /bin/sh -m www-data -c 'python3 start.py'
