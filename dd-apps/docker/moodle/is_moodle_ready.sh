#!/bin/sh -eu

# Check exclusively whether moodle has been installed and the web service is up
if test -f /var/www/html/admin/cli/isinstalled.php && \
	test -f /var/www/html/config.php && \
	! php -d max_input_vars=$max_input_vars \
		/var/www/html/admin/cli/isinstalled.php 2>&1 > /dev/null && \
	curl -s --output /dev/null localhost:8080; then
	exit 0
else
	# Force exit status 1, docker reserves exit status 2
	exit 1
fi
