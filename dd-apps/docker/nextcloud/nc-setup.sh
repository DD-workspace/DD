#!/bin/sh -eu

#
# This runs as www-data
#

occupgrade() {
	# Maintenance mode must be off
	./occ maintenance:mode --off
	# Sometimes this has to happen twice
	./occ upgrade
	./occ upgrade
}

plugin_status() {
	plugin="$1"
	plugins_state="$(./occ app:list --output=json_pretty)"
	version="$(echo "${plugins_state}" | jq -r ".enabled.${plugin}")"
	if [ "${version}" != "null" ]; then
		printf "%s\t%s" "enabled" "${version}"
	else
		version="$(echo "${plugins_state}" | jq -r ".disabled.${plugin}")"
		is_available="$(echo "${plugins_state}" | jq -r \
			--arg plugin "${plugin}" \
			'.disabled | to_entries[] | select(.key == $plugin) | .key')"
		if [ "${is_available}" = "${plugin}" ]; then
			printf "%s\t%s" "disabled" "${version}"
		else
			printf "%s\t%s" "n/a" "n/a"
		fi
	fi
}


cat <<EOF
**************************************
Performing DD-specific Nextcloud setup
**************************************

EOF

# Install static settings
echo "--> Setting up static DD config"
STATIC_CFG=/var/www/html/config/zzz_dd.config.php
cat > "${STATIC_CFG}" <<EOF
<?php
/** DD-customised static settings
  */
\$CONFIG = array(
  'default_language' => '${FORCED_LANGUAGE}',
  'force_language' => '${FORCED_LANGUAGE}',
  'skeletondirectory' => '',
  'theme' => 'dd',
  'allow_local_remote_servers' => true,
  'enabledPreviewProviders' => [
    'OC\\Preview\\PNG',
    'OC\\Preview\\JPEG',
    'OC\\Preview\\GIF',
    'OC\\Preview\\BMP',
    'OC\\Preview\\XBitmap',
    'OC\\Preview\\MP3',
    'OC\\Preview\\TXT',
    'OC\\Preview\\MarkDown',
    'OC\\Preview\\OpenDocument',
    'OC\\Preview\\Krita',
    'OC\\Preview\\Movie',
    'OC\\Preview\\MP4',
  ],
);
EOF

occupgrade

# These cannot be edited from outside of the DD project
# Operators should instead rely on the environment variables to ease deployment
# EXTRA_PLUGINS_ENABLE and EXTRA_PLUGINS_DISABLE
CORE_PLUGINS_ENABLE="user_saml,bruteforcesettings,polls,calendar,spreed,bbb,mail,onlyoffice"
CORE_PLUGINS_DISABLE="firstrunwizard,recommendations,dashboard,circles,ownpad"

if [ "${DISABLE_CLAMAV:-true}" = "false" ]; then
	CORE_PLUGINS_ENABLE="${CORE_PLUGINS_ENABLE},files_antivirus"
	USING_CLAMAV="YES"
else
	CORE_PLUGINS_DISABLE="${CORE_PLUGINS_DISABLE},files_antivirus"
fi

# Take care of installing core plugins and extra requested plugins
PLUGINS="${CORE_PLUGINS_ENABLE},${CORE_PLUGINS_DISABLE},${EXTRA_PLUGINS_ENABLE:-},${EXTRA_PLUGINS_DISABLE:-}"

# Install all plugins
# shellcheck disable=SC2086  # We do want multiple arguments
for plugin in $(echo "${PLUGINS}" | tr ',' '\n'); do
	if plugin_status "${plugin}" | grep -qE "^n/a"; then
		echo "--> Installing ${plugin}"
		./occ --no-warnings app:install "${plugin}"
	fi
done

# Enable core plugins
# shellcheck disable=SC2086  # We do want multiple arguments
for plugin in $(echo "${CORE_PLUGINS_ENABLE}" | tr ',' '\n'); do
	if plugin_status "${plugin}" | grep -qE "^disabled"; then
		echo "--> Enabling core ${plugin}"
		./occ --no-warnings app:enable "${plugin}"
	fi
done

# Disable core plugins
# shellcheck disable=SC2086  # We do want multiple arguments
for plugin in $(echo "${CORE_PLUGINS_DISABLE}" | tr ',' '\n'); do
	if plugin_status "${plugin}" | grep -qE "^enabled"; then
		echo "--> Disabling core ${plugin}"
		./occ --no-warnings app:disable "${plugin}"
	fi
done


# Enable extra plugins
# shellcheck disable=SC2086  # We do want multiple arguments
for plugin in $(echo "${EXTRA_PLUGINS_ENABLE:-}" | tr ',' '\n'); do
	if plugin_status "${plugin}" | grep -qE "^disabled"; then
		echo "--> Enabling extra ${plugin}"
		./occ --no-warnings app:enable "${plugin}"
	fi
done

# Disable extra plugins
# shellcheck disable=SC2086  # We do want multiple arguments
for plugin in $(echo "${EXTRA_PLUGINS_DISABLE:-}" | tr ',' '\n'); do
	if plugin_status "${plugin}" | grep -qE "^enabled"; then
		echo "--> Disabling extra ${plugin}"
		./occ --no-warnings app:disable "${plugin}"
	fi
done

occupgrade


## Forms
## This should be upstreamed, see:
##    https://github.com/juanan3ip/form/issues/76
## This is what is being used: https://github.com/3iPunt/nextcloud_forms
FORMS_EXPECTED_HASH="$(cat /forms.hash)"
FORMS_DIR="/var/www/html/custom_apps/forms"
FORMS_HASH=""
if [ -f "${FORMS_DIR}.hash" ]; then
	FORMS_HASH="$(cat "${FORMS_DIR}.hash")"
fi
if [ "${FORMS_EXPECTED_HASH}" != "${FORMS_HASH}" ]; then
	# Remove old plugin
	rm -rf "${FORMS_DIR}"
	# Install new one
	unzip -o /forms.zip -d /tmp
	mv "/tmp/nextcloud_forms-${FORMS_EXPECTED_HASH}" "${FORMS_DIR}"
	# Perform config / install
	npm --prefix "${FORMS_DIR}" install
	composer --ignore-platform-req=ext-dom -d"${FORMS_DIR}" install --no-dev -o
        # Generate build folder
        npm run build --prefix "${FORMS_DIR}"
	# Place hash marker
	cp /forms.hash "${FORMS_DIR}.hash"
fi
if plugin_status "${plugin}" | grep -qE "^disabled"; then
	./occ app:enable forms
fi
#
#occupgrade

#
# Apply app-specific configurations
#
echo "--> Configuring BBB"
# Host
./occ config:app:set -n bbb api.url --value="${BBB_HOST:-}"
# API Secret
./occ config:app:set -n -q bbb api.secret --value="${BBB_API_SECRET:-}"
# Disable Big Blue Button media check by default
./occ config:app:set -n bbb join.mediaCheck --value="false"
# Disable Big Blue Button listen only mode by default
# And enable option to join muted to Big Blue Button room by default
## TODO: Upstream these as toggeable settings
# shellcheck disable=SC2016  # We want these literal strings
sed -i.orig \
	-e 's/^\(\s*$room->setListenOnly(\)true\();\)$/\1false\2/' \
	-e 's/^\(\s*$room->setJoinMuted(\)false\();\)$/\1true\2/' \
	/var/www/html/custom_apps/bbb/lib/Service/RoomService.php
# Remove meeting join nextcloud bbb app dialog exclamation marks
sed -i.orig \
	-e 's/\(^\s*"Please enter your name!" : [^¡]*\)¡\?\([^!]*\)!\(.*\)$/\1\2\3/' \
	-e 's/\(^\s*"Let.s go!" : [^¡]*\)¡\?\([^!]*\)!\(.*\)$/\1\2\3/' \
	/var/www/html/custom_apps/bbb/l10n/*.json

# Settings
echo "--> Applying custom settings"

./occ --no-warnings config:app:set -n onlyoffice DocumentServerUrl --value="https://oof.$DOMAIN"
./occ --no-warnings config:app:set -n onlyoffice jwt_secret --value="secret"
./occ --no-warnings config:app:set -n onlyoffice jwt_header --value="Authorization"
./occ --no-warnings config:app:set -n onlyoffice sameTab --value="false"

# Moodle nextcloud task needs forcesave onlyoffice option
./occ --no-warnings config:app:set -n onlyoffice customizationForcesave --value="true"

# Add allow list IPs
./occ --no-warnings config:app:set -n bruteForce whitelist_1 --value='172.16.0.0/12'

# OnlyOffice
./occ --no-warnings config:app:set -n onlyoffice preview --value="true"
./occ --no-warnings config:app:set -n onlyoffice defFormats --value='{"csv":"false","doc":"true","docm":"false","docx":"true","docxf":"true","oform":"true","dotx":"false","epub":"false","html":"false","odp":"true","ods":"true","odt":"true","otp":"true","ots":"true","ott":"true","pdf":"false","potm":"false","potx":"false","ppsm":"false","ppsx":"true","ppt":"true","pptm":"false","pptx":"true","rtf":"false","txt":"false","xls":"true","xlsm":"false","xlsx":"true","xltm":"false","xltx":"true"}'
./occ --no-warnings config:app:set -n onlyoffice editFormats --value='{"csv":"true","odp":"false","ods":"false","odt":"false","rtf":"false","txt":"true"}'

if [ -n "${USING_CLAMAV:-}" ]; then
	echo "--> Configuring ClamAV"
	./occ --no-warnings config:app:set -n files_antivirus av_mode --value="daemon"
	./occ --no-warnings config:app:set -n files_antivirus av_host --value="dd-apps-clamav"
	./occ --no-warnings config:app:set -n files_antivirus av_port --value="3310"
	./occ --no-warnings config:app:set -n files_antivirus av_infected_action --value="only_log"
	./occ --no-warnings config:app:set -n files_antivirus av_stream_max_length --value="26214400"
	./occ --no-warnings config:app:set -n files_antivirus av_max_file_size --value="-1"
fi

# Allow nextcloud into other apps iframes
echo "--> Fixing CSP"
# TODO: this should be done in a different fashion
# Content-Security-Policy: frame-ancestors 'self' *.$DOMAIN;
# Content-Set-Policy: connect-src 'self -' *.$DOMAIN;
# Content-Set-Policy: img-src 'self' *. -$DOMAIN;
# Content-Set-Policy: style-src 'self'  -*.$DOMAIN;
# Content-Set-Policy: font-src 'self' * -.$DOMAIN;
sed -i \
	-E "s%'\\\\'self\\\\'',.*$%'\\\\'self\\\\'', '*.${DOMAIN}',%" \
	/var/www/html/lib/public/AppFramework/Http/ContentSecurityPolicy.php

# Add default file for moodle activities
TEMPLATEDOCX="/var/www/html/data/admin/files/template.docx"
if [ ! -f "${TEMPLATEDOCX}" ]; then
	echo "--> Copying activity template for Moodle"
	cp /template.docx "${TEMPLATEDOCX}"
	# We have to tell NC about this change
	./occ  files:scan admin
fi

# Configure logo
echo "--> Configuring logo"
# TODO: This should be a tad more dynamic
cachebuster="0"
if ./occ config:app:get theming cachebuster; then
	cachebuster="$(./occ config:app:get theming cachebuster)"
fi
./occ theming:config logo /custom/img/logo.png
./occ theming:config background /custom/img/background.png
./occ config:app:set theming cachebuster --value="$((cachebuster + 1 ))"

occupgrade

cat <<EOF
*************************************
Done with DD-specific Nextcloud setup
*************************************

EOF
