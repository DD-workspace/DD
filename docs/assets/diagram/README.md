# dd_workspace_education - diagram

This is made using [diagrams.net][diagrams].

If you want to contribute a modification,
please provide it in following formats:

- `.drawio`: as downloaded from [diagrams.net][diagrams]
- `.png`: as downloaded from [diagrams.net][diagrams],
  **without** the "include a copy of my diagram" option
- `.pdf`: as the result of running
  `convert dd_workspace_education.png dd_workspace_education.pdf`

[diagrams]: https://app.diagrams.net
